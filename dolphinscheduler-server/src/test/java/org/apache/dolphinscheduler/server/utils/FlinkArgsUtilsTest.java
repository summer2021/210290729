/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.server.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.apache.dolphinscheduler.common.enums.ProgramType;
import org.apache.dolphinscheduler.common.process.ResourceInfo;
import org.apache.dolphinscheduler.common.task.flink.FlinkParameters;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test FlinkArgsUtils
 */
public class FlinkArgsUtilsTest {

    private static final Logger logger = LoggerFactory.getLogger(FlinkArgsUtilsTest.class);

    public String mode = "cluster";
    public int slot = 2;
    public int parallelism = 3;
    public String appName = "testFlink";
    public int taskManager = 4;
    public String taskManagerMemory = "2G";
    public String jobManagerMemory = "4G";
    public ProgramType programTypeJava = ProgramType.JAVA;
    public ProgramType programTypePython = ProgramType.PYTHON;
    public String mainClass = "com.test";
    public ResourceInfo mainJar = null;
    public String mainArgs = "testArgs --input file:///home";
    public String queue = "queue1";
    public String others = "-s hdfs:///flink/savepoint-1537";
    public String flinkVersion = "<1.10";
    public ResourceInfo pyFile = null;
    public String pyFiles = "file:///tmp/myresource.zip,hdfs:///$namenode_address/myresource2.zip";
    public String pyModule = "word_count";
    public String pyExec = "/usr/local/bin/python3";
    public ResourceInfo pyReq = null;
    public List<ResourceInfo> pyArch = null;

    @Before
    public void setUp() {
        ResourceInfo main = new ResourceInfo();
        main.setRes("testflink-1.0.0-SNAPSHOT.jar");
        mainJar = main;

        ResourceInfo mockMainPy = new ResourceInfo();
        mockMainPy.setRes("testflink.py");
        pyFile = mockMainPy;

        ResourceInfo mockReq = new ResourceInfo();
        mockReq.setRes("requirement.txt");
        pyReq = mockReq;

        pyArch = new ArrayList<>();

        ResourceInfo mockArch1 = new ResourceInfo();
        mockArch1.setRes("py37.py");
        pyArch.add(mockArch1);

        ResourceInfo mockArch2 = new ResourceInfo();
        mockArch2.setRes("data.zip");
        pyArch.add(mockArch2);
    }

    /**
     * Test buildArgs with java type
     */
    @Test
    public void testBuildArgs() {
        //Define params
        FlinkParameters param = new FlinkParameters();
        param.setDeployMode(mode);
        param.setMainClass(mainClass);
        param.setAppName(appName);
        param.setSlot(slot);
        param.setParallelism(parallelism);
        param.setTaskManager(taskManager);
        param.setJobManagerMemory(jobManagerMemory);
        param.setTaskManagerMemory(taskManagerMemory);
        param.setMainJar(mainJar);
        param.setProgramType(programTypeJava);
        param.setMainArgs(mainArgs);
        param.setQueue(queue);
        param.setOthers(others);
        param.setFlinkVersion(flinkVersion);

        //Invoke buildArgs
        List<String> result = FlinkArgsUtils.buildArgs(param);
        for (String s : result) {
            logger.info(s);
        }

        //Expected values and order
        assertEquals(22, result.size());

        assertEquals("-m", result.get(0));
        assertEquals("yarn-cluster", result.get(1));

        assertEquals("-ys", result.get(2));
        assertSame(slot, Integer.valueOf(result.get(3)));

        assertEquals("-ynm", result.get(4));
        assertEquals(appName, result.get(5));

        assertEquals("-yn", result.get(6));
        assertSame(taskManager, Integer.valueOf(result.get(7)));

        assertEquals("-yjm", result.get(8));
        assertEquals(jobManagerMemory, result.get(9));

        assertEquals("-ytm", result.get(10));
        assertEquals(taskManagerMemory, result.get(11));

        assertEquals("-yqu", result.get(12));
        assertEquals(queue, result.get(13));

        assertEquals("-p", result.get(14));
        assertSame(parallelism, Integer.valueOf(result.get(15)));

        assertEquals("-sae", result.get(16));

        assertEquals(others, result.get(17));

        assertEquals("-c", result.get(18));
        assertEquals(mainClass, result.get(19));

        assertEquals(mainJar.getRes(), result.get(20));
        assertEquals(mainArgs, result.get(21));

        //Others param without -yqu
        FlinkParameters param1 = new FlinkParameters();
        param1.setQueue(queue);
        param1.setDeployMode(mode);
        result = FlinkArgsUtils.buildArgs(param1);
        assertEquals(5, result.size());
    }

    /*
     * Test BuildArgs with python type
     * */
    @Test
    public void testPyTypeArgs() {
        FlinkParameters param = new FlinkParameters();
        param.setDeployMode(mode);
        param.setAppName(appName);
        param.setSlot(slot);
        param.setParallelism(parallelism);
        param.setTaskManager(taskManager);
        param.setJobManagerMemory(jobManagerMemory);
        param.setTaskManagerMemory(taskManagerMemory);
        param.setProgramType(programTypePython);
        param.setMainArgs(mainArgs);
        param.setQueue(queue);
        param.setOthers(others);
        param.setFlinkVersion(flinkVersion);
        param.setPyFile(pyFile);
        param.setPyFiles(pyFiles);
        param.setPyModule(pyModule);
        param.setPyExec(pyExec);
        param.setPyReq(pyReq);
        param.setPyArch(pyArch);

        //Invoke buildArgs
        List<String> result = FlinkArgsUtils.buildArgs(param);
        for (String s : result) {
            logger.info(s);
        }

        //Expected values and order
        assertEquals(31, result.size());

        assertEquals("-m", result.get(0));
        assertEquals("yarn-cluster", result.get(1));

        assertEquals("-ys", result.get(2));
        assertSame(slot, Integer.valueOf(result.get(3)));

        assertEquals("-ynm", result.get(4));
        assertEquals(appName, result.get(5));

        assertEquals("-yn", result.get(6));
        assertSame(taskManager, Integer.valueOf(result.get(7)));

        assertEquals("-yjm", result.get(8));
        assertEquals(jobManagerMemory, result.get(9));

        assertEquals("-ytm", result.get(10));
        assertEquals(taskManagerMemory, result.get(11));

        assertEquals("-yqu", result.get(12));
        assertEquals(queue, result.get(13));

        assertEquals("-p", result.get(14));
        assertSame(parallelism, Integer.valueOf(result.get(15)));

        assertEquals("-sae", result.get(16));

        assertEquals(others, result.get(17));

        assertEquals("-py", result.get(18));
        assertEquals(pyFile.getRes(), result.get(19));

        assertEquals("-pyfs", result.get(20));
        assertEquals(pyFiles, result.get(21));

        assertEquals("-pym", result.get(22));
        assertEquals(pyModule, result.get(23));

        assertEquals("-pyexec", result.get(24));
        assertEquals(pyExec, result.get(25));

        assertEquals("-pyreq", result.get(26));
        assertEquals(pyReq.getRes(), result.get(27));

        assertEquals("-pyarch",result.get(28));
        StringBuilder tmp = new StringBuilder();
        int length = pyArch.size();
        for (int i = 0; i < length; ++i) {
            if (i != 0) {
                tmp.append(",");
            }
            tmp.append(pyArch.get(i).getRes());
        }
        assertEquals(tmp.toString(), result.get(29));

        assertEquals(mainArgs, result.get(30));

        //Others param without -yqu
        FlinkParameters param1 = new FlinkParameters();
        param1.setQueue(queue);
        param1.setDeployMode(mode);
        result = FlinkArgsUtils.buildArgs(param1);
        assertEquals(5, result.size());
    }

}
